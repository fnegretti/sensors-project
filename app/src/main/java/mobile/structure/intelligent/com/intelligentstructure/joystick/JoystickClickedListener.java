package mobile.structure.intelligent.com.intelligentstructure.joystick;

public interface JoystickClickedListener {
	public void OnClicked();
	public void OnReleased();
}
