package mobile.structure.intelligent.com.intelligentstructure;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import org.rajawali3d.Object3D;
import org.rajawali3d.animation.Animation;
import org.rajawali3d.animation.Animation3D;
import org.rajawali3d.animation.RotateOnAxisAnimation;
import org.rajawali3d.lights.PointLight;
import org.rajawali3d.loader.LoaderOBJ;
import org.rajawali3d.loader.ParsingException;
import org.rajawali3d.math.vector.Vector3;
import mobile.structure.intelligent.com.intelligentstructure.joystick.DualJoystickView;
import mobile.structure.intelligent.com.intelligentstructure.joystick.JoystickMovedListener;

public class LoadModelFragment extends AExampleFragment {
    LoadModelRenderer.Movers mover;

	@Override
    public AExampleRenderer createRenderer() {
		return new LoadModelRenderer(getActivity());
	}

	private final class LoadModelRenderer extends AExampleRenderer {
		private PointLight mLight;
		private Object3D mObjectGroup;
		private Animation3D mCameraAnimX,mCameraAnimY;
        Button meno, piu;
        DualJoystickView joystick;

		public LoadModelRenderer(Context context) {
			super(context);
		}

        private JoystickMovedListener _listenerLeft = new JoystickMovedListener() {
            @Override
            public void OnMoved(int pan, int tilt) {
                 System.out.println("_listenerLeft OnMoved " + pan + " " + tilt);
                if(pan <= -8 && (tilt <= 2 && tilt >= -2 ) ){
                    mover.setLeft();
                }
                if(pan <= -8 && (tilt <= -8)){
                    mover.setUpLeft();
                }
                if(pan <= 2 && pan >= -2 && tilt <= -8){
                    mover.setUp();
                }
                if(pan >= 8 && (tilt <= -8)) {
                    mover.setUpRight();
                }
                if(pan >= 8 && (tilt <= 2 && tilt >= -2 )){
                    mover.setRight();
                }
                if(pan >= 8 && tilt >=8 ){
                    mover.setDownRight();
                }
                if(pan >= -2 && pan <= 2 && tilt >= 8){
                    mover.setDown();
                }
                if(pan <= -8 && tilt >= 8){
                    mover.setDownLeft();
                }
            }
            @Override
            public void OnReleased() {
                mover.stopAll();
            }
            public void OnReturnedToCenter() {
                mover.stopAll();
            };
        };

        private JoystickMovedListener _listenerRight = new JoystickMovedListener() {
            @Override
            public void OnMoved(int pan, int tilt) {
                System.out.println("_listenerRight OnMoved "+pan+" "+tilt);
                if(pan <= -8 && (tilt <= 2 && tilt >= -2 ) ){
                    mover.setRotateLeft();
                }
                if(pan <= 2 && pan >= -2 && tilt <= -8){
                    mover.setRotateDown();
                }
                if(pan >= 8 && (tilt <= 2 && tilt >= -2 )){
                    mover.setRotateRight();
                }
                if(pan >= -2 && pan <= 2 && tilt >= 8){
                    mover.setRotateUp();
                }
            }
            @Override
            public void OnReleased() {
                mover.stopAllRotate();
            }
            public void OnReturnedToCenter() {
                mover.stopAllRotate();
            };
        };

        @Override
		protected void initScene() {
            mover = new Movers();
            new Thread(mover).start();
			mLight = new PointLight();
            mLight.setPosition(10, 10, 10);
			mLight.setPower(20);
            joystick = (DualJoystickView) getActivity().findViewById(R.id.joystickView);
            meno = (Button) getActivity().findViewById(R.id.meno);
            piu = (Button) getActivity().findViewById(R.id.piu);
            meno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCurrentCamera().setZ( getCurrentCamera().getZ() + 0.5);
                }
            });
            piu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCurrentCamera().setZ( getCurrentCamera().getZ() - 0.5);
                }
            });
            joystick.setOnJostickMovedListener(_listenerLeft, _listenerRight);
			getCurrentScene().addLight(mLight);
			getCurrentCamera().setZ(20);
			getCurrentCamera().setX(5);
			LoaderOBJ objParser = new LoaderOBJ(mContext.getResources(),mTextureManager, R.raw.untitled_obj);
			try {
				objParser.parse();
				mObjectGroup = objParser.getParsedObject();
				getCurrentScene().addChild(mObjectGroup);
				mCameraAnimY = new RotateOnAxisAnimation(Vector3.Axis.Y, 360);
				mCameraAnimY.setDurationMilliseconds(8000);
				mCameraAnimY.setRepeatMode(Animation.RepeatMode.INFINITE);
				mCameraAnimY.setTransformable3D(mObjectGroup);
				mCameraAnimX = new RotateOnAxisAnimation(Vector3.Axis.X, 360);
				mCameraAnimX.setDurationMilliseconds(8000);
				mCameraAnimX.setRepeatMode(Animation.RepeatMode.INFINITE);
				mCameraAnimX.setTransformable3D(mObjectGroup);
				getCurrentScene().setBackgroundColor(Color.WHITE);
			} catch (ParsingException e) {
                e.printStackTrace();
            }
		}

        public class Movers implements Runnable {
            private boolean moveLeft;
            private boolean moveRight;
            private boolean moveUp;
            private boolean moveDown;
            private boolean rotateLeft;
            private boolean rotateRight;
            private boolean rotateUp;
            private boolean rotateDown;
            public static final float minSpeed = 0.01f;
            public static final float maxSpeed = 0.05f;
            public float constant;

            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if(getCurrentCamera().getZ() > 12)
                        constant = maxSpeed;
                    else
                        constant = minSpeed;

                    if (moveLeft)
                        getCurrentCamera().setX(getCurrentCamera().getX() + constant);
                    if (moveRight)
                        getCurrentCamera().setX(getCurrentCamera().getX() - constant);
                    if (moveDown)
                        getCurrentCamera().setY(getCurrentCamera().getY() + constant);
                    if (moveUp)
                        getCurrentCamera().setY(getCurrentCamera().getY() - constant);

                    if (rotateDown)
                        mObjectGroup.rotate(1, 0, 0, 0.2);
                    if (rotateUp)
                        mObjectGroup.rotate(-1, 0, 0, 0.2);
                    if (rotateLeft)
                        mObjectGroup.rotate(0, 1, 0, 0.2);
                    if (rotateRight)
                        mObjectGroup.rotate(0, -1, 0, 0.2);
                }
            }

            public void stopAll(){
                moveLeft = false;
                moveRight = false;
                moveUp = false;
                moveDown = false;
            }

            public void stopAllRotate(){
                rotateLeft = false;
                rotateRight = false;
                rotateUp = false;
                rotateDown = false;
            }

            public void setRotateLeft(){
                rotateLeft = true;
            }
            public void setRotateRight(){
                rotateRight = true;
            }
            public void setRotateUp(){
                rotateUp = true;
            }
            public void setRotateDown(){
                rotateDown = true;
            }
            public void setLeft(){
                if(moveRight)
                    moveRight = false;
                moveLeft = true;
            }
            public void setUpLeft(){
                setUp();
                setLeft();
            }
            public void setDownLeft(){
                setDown();
                setLeft();
            }
            public void setRight(){
                if(moveLeft)
                    moveLeft = false;
                moveRight = true;
            }
            public void setDownRight(){
                setDown();
                setRight();
            }
            public void setUpRight(){
                setUp();
                setRight();
            }
            public void setUp(){
                if(moveDown)
                    moveDown = false;
                moveUp = true;

            }
            public void setDown(){
                if(moveUp)
                    moveUp = false;
                moveDown = true;
            }
        }
	}
}
